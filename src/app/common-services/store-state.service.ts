import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {State} from '../models/state.model';
import {Product} from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class StoreStateService {
  private stateSubject = new BehaviorSubject<State>({products: [], numberOfProducts: 0});
  private state$ = this.stateSubject.asObservable();

  constructor() {
  }

  addProducts(product: Product): void {
    const value = this.stateSubject.getValue();

    value.products.push(product);
    value.numberOfProducts = value.products.length;

  }

  resetState(): void {
    const value = this.stateSubject.getValue();
    value.numberOfProducts = 0;
    value.products = [];
  }

  setState(state: State): void {
    this.stateSubject.next(state);
  }

  getCartState(): State {
    return this.stateSubject.getValue();
  }
}
