import {Product} from './product.model';

export class Order {
  id?: string;
  orderDetails?: string;
  orderDate?: Date | string;
  orderNumber?: string;
  deliveryAddress?: string;
  products?: Product[];

  constructor(init?: Partial<Order>) {
    Object.assign(this, init);
  }
}
