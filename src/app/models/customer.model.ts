import {Address} from './address.model';
import {Order} from './order.model';

export class Customer {
  id?: string;
  name?: string;
  email?: string;
  phoneNumber?: string;
  password?: string;
  token?: string;
  authority?: string;
  addresses: Address[];
  orders: Order[];

  constructor(init?: Partial<Customer>) {
    Object.assign(this, init);
  }

}
