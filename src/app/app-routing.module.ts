import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CanActivatePage} from './security/can-activate';
import {HomeComponent} from './components/home/home.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./components/login/login.module').then(mod => mod.LoginModule)
  },
  {
    path: 'welcome',
    canActivate: [CanActivatePage],
    component: HomeComponent
  },
  {
    path: 'customer',
    loadChildren: () => import('./components/customer-components/customer.module').then(mod => mod.CustomerModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./components/admin-components/admin.module').then(mod => mod.AdminModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
