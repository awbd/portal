import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Supplier} from '../../../models/supplier.model';
import {Warehouse} from '../../../models/warehouse.model';
import {Product} from '../../../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {
  private readonly supplierUrl: string = 'supplier';

  constructor(private http: HttpClient) {
  }

  getSuppliers(pageNumber, pageSize, all): Observable<HttpResponse<Supplier[]>> {
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber);
    params = params.set('pageSize', pageSize);
    params = params.set('completeList', all);


    return this.http.get<Supplier[]>(this.supplierUrl + '/list', {params, observe: 'response'});
  }

  saveSupplier(supplier: Supplier): Observable<Supplier> {
    return this.http.post<Supplier>(this.supplierUrl, supplier);
  }

  deleteSupplier(supplier: Supplier): Observable<Supplier> {
    let params = new HttpParams();
    params = params.set('supplierId', supplier.id);

    return this.http.delete<Supplier>(this.supplierUrl, {params});
  }

  findAllWarehousesBySupplier(supplierId: string): Observable<Warehouse[]> {
    return this.http.get<Warehouse[]>(`${this.supplierUrl}/${supplierId}/warehouses`);
  }

  findAllProductsBySupplier(supplierId: string): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.supplierUrl}/${supplierId}/products`);
  }

}
