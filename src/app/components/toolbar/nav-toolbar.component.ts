import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../login/authentication.service';
import {Authorization} from '../../models/enums/authorization.enum';
import {StoreStateService} from '../../common-services/store-state.service';

@Component({
  selector: 'app-nav-toolbar',
  templateUrl: './nav-toolbar.component.html',
  styleUrls: ['./nav-toolbar.component.scss']
})
export class NavToolbarComponent implements OnInit {

  Authorization = Authorization;

  constructor(public authService: AuthenticationService,
              public storeStateService: StoreStateService) {
  }

  ngOnInit(): void {
  }
}
