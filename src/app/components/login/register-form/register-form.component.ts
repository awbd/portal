import {Component, OnInit} from '@angular/core';
import {Customer} from '../../../models/customer.model';
import {AuthenticationService} from '../authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  customer: Customer;
  registerFormGroup: FormGroup;
  errorMessage: string;

  constructor(private loginService: AuthenticationService,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.registerFormGroup = this.formBuilder.group({
      email: ['', Validators.required],
      name: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      password: ['', Validators.required],
    });
    if (localStorage.getItem('loggedCustomer')) {
      this.router.navigate(['/customer/home']);
    }
  }

  register(): void {
    this.customer = new Customer(this.registerFormGroup.value);
    this.loginService.registerCustomer(this.customer).subscribe(res => {
      this.router.navigateByUrl('/login');
    }, error => this.errorMessage = error?.error?.errorMessage);
  }
}
