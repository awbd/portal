import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressChooserComponent } from './address-chooser.component';

describe('AddressChooserComponent', () => {
  let component: AddressChooserComponent;
  let fixture: ComponentFixture<AddressChooserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddressChooserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
