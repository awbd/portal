import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Address} from '../../../models/address.model';

@Injectable({
  providedIn: 'root'
})
export class AddressService {
  private readonly addressUrl: string = 'address';

  constructor(private http: HttpClient) {
  }

  async createAddress(address: Address): Promise<Address> {
    return this.http.post(this.addressUrl, address).toPromise();
  }
}
