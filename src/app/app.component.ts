import { Component } from '@angular/core';
import {AuthenticationService} from './components/login/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'awbd-front';

  constructor(public auth: AuthenticationService) {
  }


}
